const mongoose = require('mongoose')

const chargerSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true,
    maxlength: 32,
    unique : true,
    index: true,
    dropDups: true
  },
  status: {
    type: String,
    enum: ['ready', 'charging', 'waiting', 'error'],
    required: true,
    default: 'ready'
  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now
  },
  deleted_at: {
    type: Date,
    required: false,
    default: null
  }
}, { versionKey: false })

module.exports = mongoose.model('ChargePoint', chargerSchema)
