const express = require('express')
const router = express.Router()
const ChargePoint = require('../models/chargepoint')

module.exports = function(io) {

	// Create a new charger
	router.post('/', async (req, res) => {
		
		const charger = new ChargePoint({
			id: req.body.id,
			name: req.body.name
		})

		try {
			const newCharger = await charger.save()
			res.status(201).json(newCharger)
		} catch (err) {
			res.status(400).json({ message: err.message })
		}
	})

	// Delete a charger (field deleted_at)
	router.delete('/:id', getCharger, async (req, res) => {
		
		try {
			await res.charger.remove()
			res.json({ message: 'Deleted this charge point' })
		} catch(err) {
			res.status(500).json({ message: err.message })
		}
	})

	// List of chargers (except deleted)
	router.get('/', async (req, res) => {
		
		try {
			const chargers = await ChargePoint.find()
			res.json(chargers)
		} catch (err) {
			res.status(500).json({ message: err.message })
		}
	})

	// Charger data
	router.get('/:id', getCharger, (req, res) => {
		res.json(res.charger)
	})

	// Modify charger status
	router.put('/status/:id', getCharger, async (req, res) => {
		
		if (req.body.status != null) {
			res.charger.status = req.body.status
		}

		try {
			const updatedCharger = await res.charger.save()
			await io.emit('update status', {
				status: updatedCharger.status,
				chargepoint: updatedCharger.name
			})
			res.json(updatedCharger)
		} catch (err) {
			res.status(400).json({ message: err.message })
		}
	})

	async function getCharger(req, res, next) {

		try {
			charger = await ChargePoint.findOne({ id: req.params.id })
			if (charger == null) {
				return res.status(404).json({ message: 'Cant find charge point'})
			}
		} catch (err) {
			return res.status(500).json({ message: err.message })
		}

		res.charger = charger
		next()
	}

    return router;
}

