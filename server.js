require('dotenv').config()

const express = require('express')
const http = require('http');
const path = require('path');
const app = express()
const mongoose = require('mongoose')
const server = http.Server(app);
const io = require('socket.io')(server);

mongoose.connect(process.env.DATABASE_URL, { useCreateIndex: true, useNewUrlParser: true,  useUnifiedTopology: true })
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('connected to database'))

app.use(express.json())

const chargersRouter = require('./routes/chargers')
app.use('/chargepoint', chargersRouter(io))

app.listen(3000, () => console.log('server started'))

// Web server configuration

server.listen(process.env.PORT || 8000, () => {
	console.log(`[ server.js ] Listening on port ${server.address().port}`);
});

// Web routes

app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname, 'views/show.html'));
});


// Socket Events

io.on('connection', (socket) => {
	
	console.log(`[ server.js ] ${socket.id} connected`);

	// socket.emit('update status', `Hello ${socket.id}`);

	socket.on('disconnect', () => {
		console.log(`[ server.js ] ${socket.id} disconnected`);
	});
});
