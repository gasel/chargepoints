# ChargePoints

- Node API to access some information about charge points, in JSON format.
  Thechnologies / Framworks used: NodeJS, MongoDB, Mongoose, Express.

- In the second phase websocket connection is added.
  Thechnologies / Framworks used: SocketIO.

## Installation

Install Express https://expressjs.com/en/starter/installing.html

Install Mongoose : npm i express mongoose

Install dotenv and nodemon (dotenv will allow us to pull in environment variables from a .env file and nodemon will update our local server each time we save):
npm i --save-dev dotenv nodemon

Create MongoDB database with name 'chargepoints' on the MongoDB server running at localhost:27017, and create the collection 'ChargePoint'

npm install --save socket.io

## Usage

Start server in dev environment: npm run chargepoints_dev

API can be tested with tolls as Postman, for example.

URL to check the status change on real-time:
http://localhost:8000

## Dev phases

- Create API
- Model storage
- Notifications

## Next steps

- Authentication with JWT
- Geolocation

## License
[MIT](https://choosealicense.com/licenses/mit/)
